<html>
@extends('theme::public.layout.master')
@section('content')
<body class="background-image home-image">
	<div class="home-title">
		<div class="col-md-offset-0 col-md-12">
			<div class="home-text">
				Beta Work-Up, le Portail des Auto-Entrepreneurs
			</div>
			<p>Trouver les entrepreneurs près de chez vous et gérer vos prestation en ligne</p>
		</div>
	</div>
	<div class="home-title">
		<div class="col-md-offset-0 col-md-12">
			<div class="beta-text">
				Ce site est une beta ouverte, tous les retours seront appréciés !
			</div>
		</div>
	</div>
	<div class="titre-fix">
		<div class="col-md-offset-1 col-md-2">
			<div class="icon-home-round">
				<img src="{{asset('theme/theme-work-up/public/img/logo/pen.png')}}" class="logo-home"/>
			</div>
			<div class="font-home">Gèrer vos devis en ligne</div>
		</div>
		<div class="col-md-offset-2 col-md-2">
			<div class="icon-home-round">
				<img src="{{asset('theme/theme-work-up/public/img/logo/map-point.png')}}" class="logo-home"/>
			</div>
			<div class="font-home">Trouver un Entrepreneur près de chez-vous</div>
		</div>
		<div class="col-md-offset-2 col-md-2">
			<div class="icon-home-round">
				<img src="{{asset('theme/theme-work-up/public/img/logo/technology.png')}}" class="logo-home"/>
			</div>
			<div class="font-home">Retrouvez nous sur mobile Android</div>
		</div>
	</div>
</body>
@stop

</html>