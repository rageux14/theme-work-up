<html>
  <head>
    <link rel="icon" href="{{asset('/uploads/system/favicon/favicon-1.ico')}}" sizes="16x16 32x32" type="image/vnd.microsoft.icon">
    <link rel="icon" href="{{asset('/uploads/system/favicon/favicon-16.png')}}" sizes="16x16" type="image/png">
    <link rel="icon" href="{{asset('/uploads/system/favicon/favicon-32.png')}}" sizes="32x32" type="image/png">
    <link rel="icon" href="{{asset('/uploads/system/favicon/favicon-128.png')}}" sizes="128x128" type="image/png">
    <link rel="icon" href="{{asset('/uploads/system/favicon/favicon.svg')}}" sizes="any" type="image/svg+xml">
    <link rel="icon" href="{{asset('/uploads/system/public-favicon/apple-touch-icon.png')}}" sizes="57x57" type="image/png">
    <link rel="apple-touch-icon" href="{{asset('/uploads/system/public-favicon/apple-touch-icon.png')}}" />
    <script src="{{asset(Theme::asset('js/vendor/head.min.js'))}}"></script>
    <script>
      head.js(
      '{{ asset(Theme::asset("js/vendor/modernizr.min.js")) }}',
      function() {
        @yield('scriptYepnope')                
        /* DOM Ready */
        yepnope([
          {
            load: '//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
            complete: function()
            {
              if ( !window.jQuery ) 
              {
                console.log('CDN Failed - Loading local version of jQuery.');
                yepnope("{{ asset(Theme::asset('js/vendor/jquery.min.js')) }}");
              }else{
                console.log('CDN Succeed');
              };
            }
          } , {
            test: 320 < screen.width // devices 320 and up
            , yep: [ '{{ asset(Theme::asset("js/vendor/response.min.js")) }}' ]
          } , {
            test: window.matchMedia,
            nope: ["{{ asset(Theme::asset('js/vendor/media.match.min.js')) }}"]
          } , {
            test: Modernizr.input.placeholder,
            nope: ["{{ asset(Theme::asset('js/vendor/placehold.min.js')) }}"],
            load: ["{{asset('').Bassets::show('public/js/main.js')}}"],
            complete: function(){                                        
              $(document).ready( function(){   
                masterClass.start();
              });
            }
            @yield('load_supp_js')
            @yield('headjs_end')
          }
        ]);
      });
    </script>
    <title>Work-Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    @yield('css')
    <link rel="stylesheet" href="{{ asset('').Bassets::show('public/css/main.css') }}">
    <nav class="navbar navbar-default">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="/"><img class="navbar-logo" src="{{asset('theme/theme-work-up/public/img/logo/Mobile_Icone.png')}}"/></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!--<ul class="nav navbar-nav">
       <li class="active"><a href="#">Se Connecter<span class="sr-only">(current)</span></a></li>
        <li class="active"><a href="#">S'inscrire<span class="sr-only">(current)</span></a></li>
      </ul>-->
        <ul class="nav navbar-nav navbar-right">
          @if (!Auth::check())        
            <li><a href="#" data-toggle="modal" data-target="#login"><font color = "white">Se Connecter</font></a></li>
            <li><a href="{{URL::to('sign-up')}}"><font color = "white">S'inscrire</font></a></li>
          @else
          <form class="navbar-form navbar-left" role="search" method="POST" action ="{{ URL::route('search-result')}}" >
              <div id="imaginary_container"> 
                <div class="input-group stylish-input-group">
                    <input type="text" name="searchString" class="form-control"  placeholder="Rechercher" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>  
                    </span>
              </div>
            </div>
          </form>
            <li class = "dropdown">
              <a class = "dropdown-toggle" data-toggle = "dropdown" href = "#">
                <font color = "white">
                  <img src="{{$user->getPathPicture("profil")}}" class="profile-image img-circle" color = "white"> {{$user->getName()}} <b class="caret"></b>
                </font>
              </a>
              <ul class = "dropdown-menu">
                <li><a href = "{{URL::to('/profil/'. $user->slug)}}">Profil</a></li>
                <li><a href = "{{URL::to('/profil/'. $user->slug . '/prestation')}}">Prestation</a></li>
                <li><a href = "{{URL::to('/profil/'. $user->slug . '/prestation-request')}}">Demande de Prestation</a></li>
                <li><a href = "{{URL::to('/profil/'. $user->slug . '/devis')}}">Devis</a></li>
                <li><a href = "#">Paramètres</a></li>
                <li class = "divider"></li>
                <li><a href="{{URL::to('logout')}}" >Se Déconnecter</font></a></li>
              </ul>
            </li>
          @endif              
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>
  </head>

  <body>
    <div class="container">
         @yield('content')
    </div>
         @include('theme::public.user.modals.login')
  </body>



<!-- Start Footer -->
<footer class="footer">
    <div class="container-footer">
        <div class="row">
            <div class="col-md-offset-0 col-md-5">
              <img src="/theme/theme-work-up/public/img/logo/GD_logo.png" class="logo_footer">
            </div>
            <div class="col-md-offset-0 col-md-6">
              <h6>Qui somme nous ?</h6>    
              <p class="text-muted">Le site Work-UP.fr a été réalisé par 4 étudiants passionés, dans le cadre d'un projet de fin d'étude niveau master en ingénieurie informatique.
             Ce projet consiste à faciliter la relation entre les différents Auto-Entrepreneurs avec leurs futurs clients. 
             Si vous avez des questions/remarques veuillez nous envoyer un mail à work.up@gmail.com.</p>
            </div>
        </div>
    </div>
</footer> 




</html>