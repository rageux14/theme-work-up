<html>

@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')

	<body>
    <div class="container">
      <div class="row">
        <div class="col-md-offset-0 col-md-12">
            <h1>Résultat de votre recherche</h1>
        </div>
      </div>
      
      @if ($result != null)
      @foreach ($result as $res)
      <hr>
      <div class="row">
        <a href="{{URL::to('/profil/'. $res->slug)}}" class="link-ae-profil">
          <div class="col-md-offset-1 col-md-2">
            <div class="max-image">
              <img src="{{$res->getPathPicture('profil')}}" alt="Scott Stevens" class="img-responsive img-circle">
            </div>
          </div>
          <div class="col-md-offset-0 col-md-8">
    		    <h3>{{$res->getName()}}</h3><p>
            @if ($res->isAuto())
              <i class="ae_libelle_job"></i><b>{{$res->getMetier()}}</b></br>
            @endif
              
              <i class="ae_mail"></i><b>{{$res->mail}}</br>
              <i class="ae_city"></i>{{$res->getCity()}}</b>
              </p>
          </div>
        </a>
      </div>
      @endforeach
      @elseif (is_array($result))
    </div>
    <hr>
    <div class="row">s
      <div class="col-md-offset-1 col-md-8">
        <h2>Nous sommes désolés,</br>
          <small>mais aucun résultat comprenant tous vos termes de recherche n’a pu être trouvé.</small></h2>
      </div>
    </div>
    </br>
    </br>
    <div class="row">
      <div class="col-md-offset-1 col-md-4">
        <h4>Quelques suggestions :</h4>
          <ul class="suggestions-list"><li>Vérifiez l’orthographe de vos termes.</li><li>Essayez en utilisant des synonymes ou des termes plus généraux.</li></ul>
      </div>
    </div>
    <hr>
    @endif
  </body>
@stop
</html>


