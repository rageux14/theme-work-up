<html>

@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-0 col-md-8">
                <h1>Prestation<br/>
                <small>Gérer ici vos prestations</small></h1>
            </div>
        </div>
        </br>
        </br>
        <div class="row">
            <div class="col-sm-9 col-md-12">
            <Nav tabs>
                <ul class="nav nav-tabs nav-justified" id="myNav">
                
                 <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <div class="list-group">
                            <input type="hidden" name="NbRequest" id="NbRequest" value="" />
                            <a class="list-group-item read" data-toggle="modal" data-target="ModalPrestation">
                                <span class="name" style="min-width: 120px; display: inline-block;">
                                    <span class="label label-primary">12:00:00</span>
                                    <span class="label label-primary">En cours</span><b> Personnaz Vincent</b>
                                </span>
                                <span class="titre">- Fuite de robinet</span>
                                <span class="text-muted" style="font-size: 13px; inline-block;">- Bonjour monsieur, j'ai une fuite dans ma salle de bain</span>
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>- 1000€</b></span>
                                 <span class="pull-right"> 
                                        <button type="submit" name="contacter" class="btn btn-primary btn-xs">Contacter</button>
                                        <button type="submit" name="finpresta" class="btn btn-danger btn-xs">Fin de Prestation</button>
                                </span>          
                            </a>
                            </ul>
            <Tab panes 13>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</html>
</body>

@stop
