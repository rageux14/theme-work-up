<html>

@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-0 col-md-8">
                <h1>Devis<br/>
                @if ($user->isAuto())
                <small>Génération de devis</small></h1>
                @else 
                    <small>Visualiser/Valider les devis</small></h1>
                @endif
            </div>
        </div>
        </br>
        </br>
        <div class="row">
            <div class="col-sm-9 col-md-12">
            <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" id="myNav">
                    @if ($user->isAuto())
                    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Devis à faire</a></li>
                    @else
                    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Devis en cours</a></li>
                    @endif
                    @if ($user->isAuto())
                    <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-time"></span> Devis en cours de validation</a></li>
                    @else
                    <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-time"></span> Devis refusé</a></li>
                    @endif
                    @if ($user->isAuto())
                    <li><a href="#messages" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Devis accepté</a></li>
                    @else
                    <li><a href="#messages" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Devis accepté</a></li>
                    @endif
                </ul>
            <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <div class="list-group">
                        <?php $i = 1 ?>
                        @if ($user->isAuto())
                            <?php $requests = $quotationAFaires ?>
                        @else 
                            <?php $requests = $quotationEncours ?>
                        @endif
                        @if ($requests != null)
                        
                        @foreach($requests as $request)
                            @foreach($request as $quotation)
                            @if ($user->isAuto())
                                <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalDevisAnswer{{$i}}">
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotation->getBenefitInfos()->getRedactorName()}}</b></span>
                                @else
                                <form method="post" class"form-horizontal" action="{{ URL::route('dl-devis')}}" id="postResponseDevis">
                                <div class="list-group-item read">
                                    <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotation->getBenefitInfos()->getDestinationName()}} </b></span>
                                @endif
                                    <span class="">- {{$quotation->getBenefitInfos()->title}}</span>
                                    <span class="text-muted" style="font-size: 11px;">- {{$quotation->getBenefitInfos()->description}}</span>
                                @if (!$user->isAuto())    
                                     <input type="hidden" name="user_id" value="{{$user->id}}"></input> 
                                     <input type="hidden" name="quotation_id" value="{{$quotation->id}}"></input> 
                                        <span class="pull-right">
                                            <button type="submit" name="visualiser" class="btn btn-primary btn-xs">Télécharger</button>
                                            <button type="submit" name="valider" class="btn btn-success btn-xs">Valider</button>
                                            <button type="submit" name="refuser" class="btn btn-danger btn-xs">Refuser</button>
                                        </span>
                                    </form>
                                @endif   
                                @if ($user->isAuto())
                                    <span class="badge">{{$quotation->created_at}}</span> 
                                    <span class="pull-right"></span>
                                @endif   
                                
                                    @if ($user->isAuto())
                                    </a>
                                    @else
                                </div>
                                </form>
                                </br>
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-12">
                                        <span class="glyphicon glyphicon-alert"></span>
                                        <span class="warning">Attention la validation du devis est irréversible. Nous vous recommandons de le télécharger afin d'être en accord avec la prestation de l'entrepreneur.</span>
                                    </div>
                                </div>
                                @endif
                            @if ($user->isAuto())
                            <div id="ModalDevisAnswer{{$i}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true"> 
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <header class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
                                                <h2 class="modal-title" id="mySmallModalLabel">Creation du devis</h2>
                                            </header>
                                            </br>
                                             <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="ClientFrom">{{$quotation->getBenefitInfos()->getRedactorName()}}</label>
                                                        </br>
                                                        <span for="ClientFrom">{{$quotation->getBenefitInfos()->getRedactorAdress()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <form method="post" class="form-horizontal"action="{{ URL::route('create-devis')}}" id="bookForm" >
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-2">
                                                    <label for="datedebut">Date de Début</label>
                                                    <input type='text' class="form-control text-center" id="datedebut" name="datedebut" value="" placeholder="Date Debut"/>
                                                </div>
                                                 <div class="col-md-offset-0 col-md-2">
                                                    <label for="datefin">Date de Fin</label>
                                                    <input type='text' class="form-control text-center" id="datefin" name="datefin" value="" placeholder="Date Fin"/>
                                                </div>
                                            </div>
                                        </br>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                    	<div class="input_fields_wrap" id="deleteplusbutton">
                                                        	<label for="Ressources">Ressources utilisées</label>
                                                        </div>
                                                	</div>
                                                </div>
                                            </div>
                                                <input type="hidden" name="user_id" value="{{$user->id}}"></input> 
                                                <input type="hidden" name="quotation_id" value="{{$quotation->id}}"></input> 
    											<div class="form-group">
     												<label class="col-xs-1 control-label"></label>
   												    <div class="col-xs-6">
    												    <input type="text" class="form-control" name="book_title_0" placeholder="Libelle" />
     												</div>
      												<div class="col-xs-2">
      												    <input type="number" class="form-control" name="book_isbn_0" placeholder="P.U" />
     												</div>
     												<div class="col-xs-2">
      												    <input type="number" class="form-control" name="book_price_0" placeholder="Qté" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
     												</div>
     												<div class="col-xs-1">
      												    <button type="button" class="btn btn-primary addButton addButtonLineDevis"><i class="glyphicon glyphicon-plus"></i></button>
    												</div>
 												</div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-10">
                                                        <span class="glyphicon glyphicon-alert"></span>
                                                        <span class="warning">Attention l'envoi du devis est irréversible. Nous vous recommandons de le télécharger afin d'être sûr de votre proposition. Le client devra valider ce devis afin de commencer la prestation.</span>

                                                    </div>
                                                </div>
                                                </br>
                                                <div class="row">
                                                    <div id="submit-buttons">
                                                        <div class="col-md-offset-1 col-md-8">
                                                            <button type="submit" name="visualiser" id="send-devis" class="btn btn-primary">Télécharger et envoyer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <?php $i++ ?>
                            @endforeach
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="profile">
                        <div class="list-group">               
                            <?php $i = 1 ?>
                            @if (!$user->isAuto())
                                <?php $requests = $quotationRefuses ?>
                             @else 
                                <?php $requests = $quotationEncours ?>
                        @endif
                        @if ($requests != null)
                        
                        @foreach($requests as $quotationEncour)
                            @foreach($quotationEncour as $quotationE)
                            <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalDevisAnswer{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotationE->getBenefitInfos()->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotationE->getBenefitInfos()->getDestinationName()}} </b></span>
                                @endif 
                                    <span class="">- {{$quotationE->getBenefitInfos()->title}}</span>
                                    <span class="text-muted" style="font-size: 11px;">- {{$quotationE->getBenefitInfos()->description}}</span> 
                                    <span class="badge">{{$quotationE->created_at}}</span> 
                                    <span class="pull-right"></span>
                                </a>
                           
                            <?php $i++ ?>
                            @endforeach
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="messages">
                        <div class="list-group">               
                            
                             @if ($quotationAcceptes != null)   
                             <?php $i = 1 ?>          
                            @foreach($quotationAcceptes as $quotationAccepte)
                            @foreach($quotationAccepte as $quotationA)
                            <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalDevisAnswer{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotationA->getBenefitInfos()->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$quotationA->getBenefitInfos()->getDestinationName()}} </b></span>
                                @endif 
                                    <span class="">- {{$quotationA->getBenefitInfos()->title}}</span>
                                    <span class="text-muted" style="font-size: 11px;">- {{$quotationA->getBenefitInfos()->description}}</span> 
                                    <span class="badge">{{$quotationA->created_at}}</span> 
                                    <span class="pull-right"></span>
                                </a>
                            <?php $i++ ?>
                            @endforeach
                            @endforeach
                            @endif
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
</html>
