<html>

@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-0 col-md-8">
                <h1> Demande de Prestation<br/>
                <small>Gérer ici vos demandes de prestation</small></h1>
            </div>
        </div>
        </br>
        </br>
        <div class="row">
            <div class="col-sm-9 col-md-12">
            <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" id="myNav">
                    <li class="active"><a href="#demande" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Demande</a></li>
                    <li><a href="#negociation" data-toggle="tab"><span class="glyphicon glyphicon-time"></span>  Négociation</a></li>
                    <li><a href="#refusee" data-toggle="tab"><span class="glyphicon glyphicon-remove"></span>  Refusée</a></li>
                    <li><a href="#terminee" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Validée</a></li>
                </ul>
            <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="demande">
                        <div class="list-group">
                        <?php $i = 1 ?>
                            @if ($user->isAuto())
                                <?php $requests = $requestEnCours ?>
                            @else 
                                <?php $requests = $requestNegociations ?>
                            @endif
                            @if ($requests != null)
                                @foreach($requests as $request)
                                <input type="hidden" name="NbRequestEnCours" id="NbRequestEnCours" value="{{$requestNbEnCours}}" />
                                <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalPrestationAnswer{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$request->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$request->getDestinationName()}} </b></span>
                                @endif 
                                    <span class="">- {{$request->title}}</span>
                                    <span class="text-muted" style="font-size: 11px;">- {{$request->description}}</span> 
                                    <span class="badge">{{$request->created_at}}</span> 
                                    <span class="pull-right"></span>
                                </a>
                            <div id="ModalPrestationAnswer{{$i}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <header class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
                                                <h2 class="modal-title" id="mySmallModalLabel">Demande de Prestation</h2>
                                            </header>
                                            </br>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                        <label for="TitleDemande" style="font-size: 20px;" >{{$request->title}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                        <label for="MessageFrom">De : {{$request->getRedactorName()}} ({{$request->getRedactorMail()}})</label></br>
                                                        <label for="MessageFrom">A : {{$request->getDestinationName()}} ({{$request->getDestinationMail()}})</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="PhoneNumber">Téléphone : </label>
                                                        <span for="City">{{$request->getRedactorPhone()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="TextMessage">Message : </label>
                                                            </br>
                                                            <span for="TextMessage">{{$request->description}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="City">Ville : </label>
                                                            <span for="City">{{$request->city}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="TimeWanted">Début Souhaité : </label>
                                                            <span for="TimeWanted">{{$request->debut_date}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="BudgetWanted">Budget : </label>
                                                            <span for="BudgetWanted">{{$request->budget}} €</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($request->hasDetails())
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="AErequest">Question au client : </label>
                                                        <span for="Aerequest">{{$request->getDetailsMessage()}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                @endif
                                                @if (!$user->isAuto() && $request->hasDetails())
                                                <div class="row">
                                                    <form action="{{ URL::route('more-informations-response')}}" method="POST">
                                                        <input type="hidden" name="prestation_id" value="{{$request->id}}"/>
                                                        <input type="hidden" name="user" value="{{User::getIdByAuth()}}">
                                                        <div class="col-md-offset-1 col-md-8">
                                                            <div class="form-group">
                                                                <label for="Clientrequest">Réponse : </label>
                                                            </div>
                                                        </div>  
                                                        <div class="col-md-offset-1 col-md-8">
                                                            <textarea name="text-response" class="form-control" for="Clientrequest"></textarea>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary pull-left">Envoyer</button>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                                @endif
                                            </br>
                                                @if ($request->hasDetails() && $user->isAuto())
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-8">
                                                        <div class="form-group">
                                                            <label for="Clientanswer">Réponse du client : </label>
                                                        <span for="Clientanswer">{{$request->getDetailsResponse()}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                @endif
                                                <div style="display:none" id="mestrucs" class="mestrucs{{$i}}">
                                                    <form action="{{ URL::route('more-informations')}}" method="POST">
                                                        <input type="hidden" name="prestation_id" value="{{$request->id}}"/>
                                                        <input type="hidden" name="user" value="{{User::getIdByAuth()}}">
                                                        <div class="row">
                                                            <div class="col-md-offset-1 col-md-8">
                                                                <div class="form-group">
                                                                    <label for="BudhetWanted">Posez votre question : </label>
                                                                </div>
                                                            </div>  
                                                            <div class="col-md-offset-1 col-md-8">
                                                                <textarea name="text" class="form-control" for="BudhetWanted"></textarea>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary pull-left">Envoyer</button>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <hr>
                                                 @if ($user->isAuto())
                                                <div class="row">
                                                    <form action="{{ URL::route('ask-benefit-request')}}" method="POST">
                                                        <input type="hidden" name="prestation_id" value="{{$request->id}}"/>
                                                        <input type="hidden" name="user" value="{{User::getIdByAuth()}}">
                                                        <div id="supp-button" class="supp-button{{$i}}">
                                                            <div class="col-md-offset-1 col-md-9">
                                                                <div class="btn-btn" role="group" aria-label="...">
                                                                    <button name="valider"type="submit" class="btn btn-primary">Valider</button>
                                                                    @if(!$request->hasDetails())
                                                                    <button onclick="addBox({{$i}})" class="btn btn-primary more-info" id="test">Plus d'informations</button>
                                                                    @endif
                                                                    <button name="refuser"type="submit" class="btn btn-danger">Refuser</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>    
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++ ?>
                            @endforeach
                        @else
                            <div class="list-group-item">
                                <span class="text-center">Vide</span>
                            </div>
                        @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="negociation">
                        <div class="list-group">
                             <?php $i = 1 ?>
                             @if (!$user->isAuto())
                                <?php $requests = $requestEnCours ?>
                            @else 
                                <?php $requests = $requestNegociations ?>
                            @endif
                            @if ($requests != null)
                                @foreach($requests as $request)
                            <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalPrestationNego{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$request->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$request->getDestinationName()}} </b></span>
                                @endif
                                <span class="">- {{$request->title}}</span>
                                <span class="text-muted" style="font-size: 11px;">- {{$request->description}}</span> 
                                <span class="badge">{{$request->created_at}}</span> 
                                <span class="pull-right"></span>
                            </a>

                            <div id="ModalPrestationNego{{$i}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <header class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
                                                <h2 class="modal-title" id="mySmallModalLabel">Prestation en cours de négociation</h2>
                                            </header>
                                            </br>
                                             <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                        <label for="MessageFrom">De : {{$request->getRedactorName()}} ({{$request->getRedactorMail()}})</label></br>
                                                        <label for="MessageFrom">A : {{$request->getDestinationName()}} ({{$request->getDestinationMail()}})</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="PhoneNumber">Téléphone : </label>
                                                        <span for="City">{{$request->getRedactorPhone()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TextMessage">Message : </label>
                                                        </br>
                                                        <span for="TextMessage">{{$request->description}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="City">Ville : </label>
                                                        <span for="City">{{$request->city}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TimeWanted">Début Souhaité : </label>
                                                        <span for="TimeWanted">{{$request->debut_date}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="BudgetWanted">Budget : </label>
                                                        <span for="BudgetWanted">{{$request->budget}} €</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="AErequest">Question au client : </label>
                                                    <span for="Aerequest">{{$request->getDetailsMessage()}}</span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @if (!$user->isAuto())
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="AErequest">Réponse envoyée à l'auto-entrepreneur : </label>
                                                    <span for="Aerequest">{{$request->getDetailsResponse()}}</span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                    @else
                        <div class="list-group-item">
                            <span class="text-center">Vide</span>
                        </div>
                    @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="refusee">
                         <div class="list-group">
                             <?php $i = 1 ?>
                        @if ($requestRefuses != null)
                            @foreach($requestRefuses as $requestRefuse)
                            <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalPrestationFinish{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$requestRefuse->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$requestRefuse->getDestinationName()}} </b></span>
                                @endif
                                <span class="">- {{$requestRefuse->title}}</span>
                                <span class="text-muted" style="font-size: 11px;">- {{$requestRefuse->description}}</span> 
                                <span class="badge">{{$requestRefuse->created_at}}</span> 
                                <span class="pull-right"></span>
                            </a>

                            <div id="ModalPrestationFinish{{$i}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <header class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
                                                <h2 class="modal-title" id="mySmallModalLabel">Prestation refusées</h2>
                                            </header>
                                            </br>
                                             <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                        <label for="MessageFrom">De : {{$requestRefuse->getRedactorName()}} ({{$requestRefuse->getRedactorMail()}})</label></br>
                                                        <label for="MessageFrom">A : {{$requestRefuse->getDestinationName()}} ({{$requestRefuse->getDestinationMail()}})</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="PhoneNumber">Téléphone : </label>
                                                        <span for="City">{{$requestRefuse->getRedactorPhone()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TextMessage">Message : </label>
                                                        </br>
                                                        <span for="TextMessage">{{$requestRefuse->description}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="City">Ville : </label>
                                                        <span for="City">{{$requestRefuse->city}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TimeWanted">Début Souhaité : </label>
                                                        <span for="TimeWanted">{{$requestRefuse->debut_date}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="BudgetWanted">Budget : </label>
                                                        <span for="BudgetWanted">{{$requestRefuse->budget}} €</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                    @else
                        <div class="list-group-item">
                            <span class="text-center">Vide</span>
                        </div>
                    @endif
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="terminee">
                        <div class="list-group">
                            <?php $i = 1 ?>
                        @if ($requestAcceptes != null)
                            @foreach($requestAcceptes as $requestAccepte)
                            <a href="#" class="list-group-item read" data-toggle="modal" data-target="#ModalPrestationDone{{$i}}">
                                @if ($user->isAuto())
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$requestAccepte->getRedactorName()}} </b></span>
                                @else
                                <span class="name" style="min-width: 120px; display: inline-block;"><b>{{$requestAccepte->getDestinationName()}} </b></span>
                                @endif 
                                <span class="">- {{$requestAccepte->title}}</span>
                                <span class="text-muted" style="font-size: 11px;">- {{$requestAccepte->description}}</span> 
                                <span class="badge">{{$requestAccepte->created_at}}</span> 
                                <span class="pull-right"></span>
                            </a>

                            <div id="ModalPrestationDone{{$i}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <header class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
                                                <h2 class="modal-title" id="mySmallModalLabel">Demandes terminées</h2>
                                            </header>
                                            </br>
                                             <div class="row">
                                                <div class="col-md-offset-1 col-md-12">
                                                    <div class="form-group">
                                                        <label for="MessageFrom">De : {{$requestAccepte->getRedactorName()}} ({{$requestAccepte->getRedactorMail()}})</label></br>
                                                        <label for="MessageFrom">A : {{$requestAccepte->getDestinationName()}} ({{$requestAccepte->getDestinationMail()}})</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="PhoneNumber">Téléphone : </label>
                                                        <span for="City">{{$requestAccepte->getRedactorPhone()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TextMessage">Message : </label>
                                                        </br>
                                                        <span for="TextMessage">{{$requestAccepte->description}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="City">Ville : </label>
                                                        <span for="City">{{$requestAccepte->city}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="TimeWanted">Début Souhaité : </label>
                                                        <span for="TimeWanted">{{$requestAccepte->debut_date}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-offset-1 col-md-8">
                                                    <div class="form-group">
                                                        <label for="BudgetWanted">Budget : </label>
                                                        <span for="BudgetWanted">{{$requestAccepte->budget}} €</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                    @else
                        <div class="list-group-item">
                            <span class="text-center">Vide</span>
                        </div>
                    @endif
                        </div>
                        @if ($user->isAuto())
                            <div class="col-md-offset-4 col-md-4">
                                <a type="button" href="{{URL::to('/profil/'. $user->slug . '/devis')}}" class="btn btn-primary btn">Générer vos Devis<span class="glyphicon glyphicon-menu-right"></span></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
</html>


