@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')
<div class="container">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<h1> Inscription Client<br/>
		    <small> Merci de renseigner vos informations </small></h1>
		    <div class="buttonAE">
				<a type="button" href="{{URL::to('sign-up-ae')}}" class="btn btn-primary btn-lg">Inscription AutoEntrepreneur<span class="glyphicon glyphicon-menu-right"></span></a>
			</div>
		</div>
	</div>
	<br/>

	 @if (Session::has('error_login'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('error_login') }}
        </div>
    @endif

	{{ Form::open(array('action' => 'ModuleWorkUpRegisterController@add')) }}
	<input type="hidden"  name="typeInscription"  value="C"/>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="form-group">
				<label for="Nom">Nom*</label>
				<input type="text" class="form-control" name="nom" value="{{Input::old('nom')}}" placeholder="Nom">
				{{ $errors->first('nom', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="form-group">
				<label for="Prenom">Prénom*</label>
				<input type="text" class="form-control" id="prenom" name="prenom" value="{{Input::old('prenom')}}"  placeholder="Prénom">
				{{ $errors->first('prenom', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="form-group">
    	<div class="col-md-offset-2 col-md-3">
            <div class="form-group">
            	<label for="Birthday">Date de Naissance</label>
                <div class='input-group date' id='birthday'>
                	<span class="input-group-addon glyphicon glyphicon-calendar"></span>
                    <input type='text' class="form-control text-center" id="date" name="date" value="{{Input::old('date')}}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
      	<div class="col-md-offset-1 col-md-3">
      		<label for="Gender">Sexe</label>
            <select class="form-control" id='sexe' name="sexe" value="{{Input::old('sexe')}}">
            	<option value="">Veuillez renseigner votre sexe...</option>
            	<option value="male">Homme</option>
                <option value="female">Femme</option>
            </select>
        </div>
    </div>
	</br>
	</div>

	<div class="row">
		<div class="col-md-offset-2 col-md-7">
			<div class="form-group">
				<label for="email">Adresse Email*</label>
				<input type="text" class="form-control" id="email" name="email" value="{{Input::old('email')}}"placeholder="Email">
				{{ $errors->first('email', '<div class="alert alert-danger">:message</div>') }}			
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="form-group">
				<label for="Password">Mot de passe*</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
				{{ $errors->first('password', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="form-group">
				<label for="Vpassword">Vérification mot de passe*</label>
				<input type="password" class="form-control" id="vpassword" name="vpassword" placeholder="Vérification mot de passe">
			</div>
		</div>
	</div>

	</br>

	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-earphone"></span>
				<input type="text" class="form-control" id="phone" name="phone" value="{{Input::old('phone')}}" placeholder="Téléphone" aria-describedby="basic-addon1">
			</div>
			{{ $errors->first('phone', '<div class="alert alert-danger">:message</div>') }}
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-globe"></span>
				<input type="text" class="form-control" id="address" name="adress" value="{{Input::old('adress')}}" placeholder="Adresse"  aria-describedby="basic-addon1">
			</div>
			{{ $errors->first('adress', '<div class="alert alert-danger">:message</div>') }}
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-home"></span>
				<input type="text" class="form-control" id="postal_code" name="postal_code" value="{{Input::old('postal_code')}}" placeholder="Code Postal" aria-describedby="basic-addon1">
			</div>
			{{ $errors->first('postal_code', '<div class="alert alert-danger">:message</div>') }}
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-home"></span>
				<input type="text" class="form-control" id="city" name="city" value="{{Input::old('city')}}" placeholder="Ville" aria-describedby="basic-addon1">
			</div>
			{{ $errors->first('city', '<div class="alert alert-danger">:message</div>') }}
		</div>
	</div>
	<br/>
	</br>
	<div class="row">
		<div class="col-md-offset-4 col-md-1">
			<button type="submit" class="btn btn-primary">Envoyer mes informations</button>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<a href="{{URL::to('sign-up-ae')}}">Vous êtes un Auto-Entrepreneur ?</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<a href="{{URL::to('forgot-password')}}">Mot de passe oublié ?</a>
		</div>
	</div>
</div>
{{ Form::close() }}
</div>

@stop