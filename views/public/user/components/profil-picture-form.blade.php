<form action="{{ URL::route('photo-profil')}}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="user_id" value="{{User::getIdByAuth()}}" />
	<div class="{{{ $errors->has('attachment') ? 'has-error' : '' }}}">
	    <input type="file" name="image" id="image" class="filestyle" data-icon="false" data-buttonText="Choisir un fichier" placeholder="Pièce jointe" value="{{Input::old('attachment')}}" {{(isset($isDisabled)?$isDisabled:'')}}>
	    {{ $errors->first('attachment', '<div class="alert alert-danger">:message</div>') }}
	    <button type="submit">Envoyer </button>
	</div>
</form>
<br>
