{{ Form::open(array('route' => 'public.login.post')) }}

    <div class="form-group {{{ $errors->has('login_email') ? 'has-error' : '' }}}">
        <label class="col-sm-3 control-label" for="login_email">Email</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="Entrez votre adresse email" type="email" name="email" id="email" value="{{ Input::old('login_pseudo') }}">
            {{ $errors->first('login_email', '<p class="text-danger">Vous devez renseigner une adresse mail valide</p>') }}
        </div>
    </div>

    <div class="form-group {{{ $errors->has('login_password') ? 'has-error' : '' }}}">
        <label class="col-sm-3 control-label" for="login_password">
            Mot de passe
        </label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="Saisissez votre mot de passe" type="password" name="password" id="password">
            {{ $errors->first('login_password', '<p class="text-danger">Vous devez indiquer un mot de passe correct</p>') }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">{{{ Lang::get('public.validate') }}}</button>
           <!-- <a class="btn btn-default" href="{{ URL::route('reminder') }}">Mot de passe oublié</a> -->
        </div>
    </div>

{{ Form::close() }}