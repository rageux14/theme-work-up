{{ Form::open(array('route' => 'public.login.post', 'class' => 'login-form authuser-form')) }}

    <div class="form-group {{{ $errors->has('login_email') ? 'has-error' : '' }}}">
        <input class="form-control" placeholder="Entrez votre adresse email" type="email" name="email" id="email" value="{{ Input::old('login_pseudo') }}">
        {{ $errors->first('login_email', '<p class="text-danger">Vous devez renseigner une adresse mail valide</p>') }}
    </div>

    <div class="form-group {{{ $errors->has('login_password') ? 'has-error' : '' }}}">
        <input class="form-control" placeholder="Saisissez votre mot de passe" type="password" name="password" id="password">
        {{ $errors->first('login_password', '<p class="text-danger">Vous devez indiquer un mot de passe correct</p>') }}
    </div>

    <p class="text-right">
        <a href="{{ URL::route('reminder') }}">{{ I18n::get('auth.forgot_password')}}</a>
    </p>

    <button type="submit" class="btn btn-primary shake"><span class="authuser-awesome-icon authuser-awesome-icon-check icon-check-mark"></span><span class="authuser-awesome-icon authuser-awesome-icon-rejected icon-remove"></span><span class="authuser-awesome-icon authuser-awesome-icon-spinner icon-fontawesome-webfont-101"></span></button>
    <!-- <button type="submit" class="btn btn-primary">{{{ Lang::get('public.validate') }}}</button> -->

{{ Form::close() }}