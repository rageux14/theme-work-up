@extends('theme::public.layout.master')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<h1> Mot de passe oublié <br/>
		</div>
	</div>
	<br/>
	<br/>

	 @if (Session::has('error_login'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('error_login') }}
        </div>
    @endif

	{{ Form::open(array('action' => 'ModuleWorkUpRegisterController@add')) }}
	<div class="row">
		<div class="col-md-offset-2 col-md-7">
			<div class="form-group">
				<label for="email">Adresse Email*</label>
				<input type="text" class="form-control" id="email" name="email" value="{{Input::old('email')}}"placeholder="Email">
				{{ $errors->first('email', '<div class="alert alert-danger">:message</div>') }}			
			</div>
		</div>
	</div>
	@include('theme::public.session.session-message')
	<div class="row">
		<div class="col-md-offset-4 col-md-1">
			<button type="submit" class="btn btn-primary">Envoyer mes informations</button>
		</div>
	</div>
{{ Form::close() }}
</div>

@stop