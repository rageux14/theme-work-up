@extends('theme::public.layout.master')

@section('content')

<div class="container">


      <!--  @if(Option::get('enable_registration') == true)
            @include('registration::components.oauth')
            <div class="col-md-6">
        @else
            <div class="col-md-12">
        @endif
        -->

           <div class="clearfix"></div>

            @if (Session::has('error_login'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('error_login') }}
                </div>
            @endif
             
             <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                <div class="container">
                    <div class="fb-profile">
                    <img align="left" class="fb-image-lg-public" src="{{$userPublic->getPathPicture("couverture")}}" alt="Cover picture"/>
                    <img align="left" class="fb-image-profile-public thumbnail" src="{{$userPublic->getPathPicture("profil")}}" alt="Profil picture"/>
                        <div class="fb-profile-text">
                            <h1>{{$userPublic->getName()}}
                            @if ($userPublic->isAuto()) 
                                <IMG class="logo-AE" src="http://fotomelia.com/wp-content/uploads/edd/2015/03/cl%C3%A9-bricolage-m%C3%A9canicien-1560x1560.png">
                            @endif
                                </h1>
                            @if ($userPublic->isAuto())    
                                <p>{{$userPublic->getCompagny()}}</p>
                            @endif        
                            <p><b>{{$userPublic->getCity()}}</b></p>
                            @if (Auth::check())
                            <div class="col-md-offset-1 col-md-0">
                                <button type="submit" class="btn btn-primary"  id="ask-contact">Contacter</button>
                            
                            @endif                           
                            @if ($userPublic->isAuto())
                                @if (Auth::check())
                                    <button type="submit" class="btn btn-primary" id="ask-benefit">Demande de prestation</button>
                                @else
                                   <b> Connectez-vous ou inscrivez vous pour contacter ou demander une prestation à l'auto entrepreneur ! </b>
                                @endif
                            @endif
                            </div>
                        </div>
                    </div>
                </div> <!-- /container -->
                @if ($userPublic->isAuto()) 
                <div class="col-md-offset-1 col-md-5">
                    <div class="form-group">
                        <label for="comment">Description:</label>
                        <p class="text-justify">{{$userPublic->getDescription()}}</p>
                    </div>
                @endif
        </div>
</div>
</div>
</div>
</div>
</div>

<div id="ModalPrestation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalPrestation" aria-hidden="true">
    <div class="modal-dialog">
    <form action="{{ URL::route('ask-benefit')}}" method="POST" enctype="multipart/form-data" class="form-benefit-ask" id="form-benefit-ask">
        <input type="hidden" name="userPublic" value="{{$userPublic->id}}">
        <input type="hidden" name="user" value="{{User::getIdByAuth()}}">
        <div class="modal-content">
            <header class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
            <h2 class="modal-title" id="mySmallModalLabel">Demande de prestation</h2>
            </header>
                </br>
                <div class="col-md-offset-0 col-md-8">
                        <p><b>{{BenefitRequest::generateBenefitNumberForHuman()}}</b></p>
                </div>
                </br>
                </br>
                <div class="row">
                    <div class="col-md-offset-1 col-md-8">
                        <div class="form-group">
                            <label for="comment">Description:</label>
                        <textarea name="description" class="form-control" rows="4" cols="50" id="comment"></textarea>
                        </div>
                        {{ $errors->first('description', '<div class="alert alert-danger">:message</div>') }}
                    </div>
                </div>
                <hr>
                    <div class="row">
                    <div class="col-md-offset-1 col-md-4">
                        <div class="form-group">
                        <label for="DatePrestation">Début Souhaité*</label>
                            <div class='input-group date' id='DatePrestation'>
                                <span class="input-group-addon glyphicon glyphicon-time"></span>
                                <input type='text' class="form-control text-center" id="date" name="date" value="" />
                            </div>
                            {{ $errors->first('date', '<div class="alert alert-danger">:message</div>') }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                    <div class="form-group">
                        <label  for="BudgetPrestation">Budget (en Euros)</label>
                        <div class="input-group"> 
                            <span class="input-group-addon">€</span>
                            <input name="budgetPrestation" id="budgetPrestation" type="number" value="1000.00" min="0" step="100" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" />
                        </div>
                        {{ $errors->first('budgetPrestation', '<div class="alert alert-danger">:message</div>') }}
                    </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-md-offset-1 col-md-4">
                            <label for="DatePrestation">Localisation*</label>
                            <div class="input-group">
                                <span class="input-group-addon glyphicon glyphicon-home"></span>
                                <input type="text" class="form-control" name="ville" id="ville"  aria-describedby="basic-addon1" value="{{$user->city}}">
                            </div>
                            {{ $errors->first('ville', '<div class="alert alert-danger">:message</div>') }}
                        </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-md-offset-9 col-md-2">
                        <div class="ButtonPrestation">
                            <button class="btn btn-primary" type="submit">Envoyer</button>
                        </div>   
                    </div>
                    @include('theme::public.session.session-message')
                </div>
            </br>
            </div>
        </form>
    </div>
</div>

<div id="ModalContact" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalContact" aria-hidden="true">
    <div class="modal-dialog">
    <form action="{{ URL::route('post-contact')}}" method="POST" enctype="multipart/form-data" class="sendprestation-form" id="form-contact-ask">
        <input type="hidden" name="userPublic" value="{{$userPublic->id}}">
        <input type="hidden" name="user" value="{{User::getIdByAuth()}}">
        <div class="modal-content">
            <header class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close Modal</span></button>
            <h2 class="modal-title" id="mySmallModalLabel">Contacter l'auto Entrepreneur</h2>
            </header>
                </br>
                <div class="row">
                    <div class="col-md-offset-1 col-md-8">
                        <div class="form-group">
                            <label for="title">Objet de la demande</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="title" id="title"  aria-describedby="basic-addon1" value="">
                            </div>
                        </div>
                        {{ $errors->first('title', '<div class="alert alert-danger">:message</div>') }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-offset-1 col-md-8">
                        <div class="form-group">
                            <label for="message">message :</label>
                        <textarea name="message" class="form-control" rows="4" cols="50" id="comment"></textarea>
                        </div>
                        {{ $errors->first('message', '<div class="alert alert-danger">:message</div>') }}
                    </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-md-offset-9 col-md-2">
                        <div class="ButtonPrestation">
                            <button class="btn btn-primary" type="submit">Envoyer</button>
                        </div>   
                    </div>
                    @include('theme::public.session.session-message')
                </div>
            </br>
            </div>
        </form>
    </div>
</div>


@stop