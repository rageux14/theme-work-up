<html>
@extends('theme::public.layout.master')

@section('content')
<body>

<div class="container">

        @include('theme::public.session.session-message')

      <!--  @if(Option::get('enable_registration') == true)
            @include('registration::components.oauth')
            <div class="col-md-6">
        @else
            <div class="col-md-12">
        @endif
        -->

           <div class="clearfix"></div>

            @if (Session::has('error_login'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('error_login') }}
                </div>
            @endif
    
             {{ Form::open(array('action' => 'ProfilController@updateInfo')) }}
             @if ($user->isAuto()) 
             <input type="hidden" name="type" value="ae" />
             @else
             <input type="hidden" name="type" value="cu" />
             @endif
             <input type="hidden" name="user_id" value="{{$user->id}}" />
             <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                <div class="container">
                    <div class="fb-profile">
                        <div class="profil-cover-picture">
                            <img align="left" class="fb-image-lg" src="{{$user->getPathPicture("couverture")}}" alt="Profile image example" data-toggle="modal" data-target="#ModalProfileCouverture"/>
                        </div>
                        <div class="profil-picture">
                            <img align="left" class="fb-image-profile thumbnail" src="{{$user->getPathPicture("profil")}}" alt="Profile image example" data-toggle="modal" data-target="#ModalProfile"/>
                        </div>
                        <div class="fb-profile-text">
                            <h1>{{$user->getName()}}
                                
                            @if ($user->isAuto())  
                                <IMG class="logo-AE" src="http://fotomelia.com/wp-content/uploads/edd/2015/03/cl%C3%A9-bricolage-m%C3%A9canicien-1560x1560.png">
                            </h1>
                            <p>{{$user->getCompagny()}}</p>
                            <p>{{$user->getMetier()}}</p>
                            @endif
                            </h1>
                            <p><b>{{$user->getCity()}}</b></p>
                            </br>
                            </br>                              
                            @if ($user->isAuto())
                        </br>
                                <div class="col-md-offset-1 col-md-5">
                                    <div class="form-group">
                                        <label for="comment">Description:</label>
                                        <textarea name="description" class="form-control" rows="5" id="comment">{{$user->getPrivateDescription()}}</textarea>
                                    </div>
                                </div>
                            @endif
                        
                    </div>
                </div> <!-- /container -->     
            </div>
    </div>
    <hr>
    <div class="col-md-offset-1 col-md-5"> 
        <div class="hero-unit">
            <h3>Informations Personnelles</h3>
        </br>
        </div>
    </div>
    <br>
    <br>
        <div class="row">
            <div class="col-md-offset-2 col-md-7">
            </div>
        </div>
    </br>
    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <label for="Téléphone">Téléphone</label>
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-earphone"></span>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Téléphone" aria-describedby="basic-addon1" value="{{$user->phone}}">
            </div>
            {{ $errors->first('phone', '<div class="alert alert-danger">:message</div>') }}
        </div>
        <div class="col-md-offset-1 col-md-3">
            <label for="Adresse">Adresse</label>
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-globe"></span>
                <input type="text" class="form-control" name="adress" id="address" placeholder="Adresse" aria-describedby="basic-addon1" value="{{$user->address}}">
            </div>
            {{ $errors->first('adress', '<div class="alert alert-danger">:message</div>') }}
        </div>
    </div>

    </br>
    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <label for="Code_postal">Code Postal</label>
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-home"></span>
                <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Code Postal" aria-describedby="basic-addon1" value="{{$user->postal_code}}">
            </div>
            {{ $errors->first('postal_code', '<div class="alert alert-danger">:message</div>') }}
        </div>
        <div class="col-md-offset-1 col-md-3">
            <label for="Ville">Ville</label>
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-home"></span>
                <input type="text" class="form-control" id="city" name="city" placeholder="City" aria-describedby="basic-addon1" value="{{$user->city}}">
                
            </div>
            {{ $errors->first('city', '<div class="alert alert-danger">:message</div>') }}
        </div>
    </div>
    </br>
    @if ($user->isAuto())  
    </br>
    <div class="row">
        <div class="col-md-offset-2 col-md-7">
            <label for="compagny">Nom de la Société</label>
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-briefcase"></span>
                <input type="text" class="form-control" id="compagny" name="compagny" placeholder="Label" aria-describedby="basic-addon1" value="{{$user->getCompagny()}}">
            </div>
            {{ $errors->first('compagny', '<div class="alert alert-danger">:message</div>') }}
        </div>
    </div>
    @endif
</br>
</br>
<div class="row">
    <div class="col-md-offset-2 col-md-3">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#Modalmotdepasse">Changer de Mot de passe ?</button>
    </div>
    <div class="col-md-offset-1 col-md-3">
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
    </div>
    <br/>
    <hr>
    {{Form::close()}}
<!-- /ModalProfile --> 
<div id="ModalProfile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <h2>Modification de votre photo de profil</h2>
            <form action="{{ URL::route('photo-profil')}}" method="POST" enctype="multipart/form-data" class="ProfilPicture-form">
                <div><img src="{{$user->getPathPicture("profil")}}" class="modal-profil-pic"></div>
                </br>
                <input type="hidden" name="user_id" value="{{User::getIdByAuth()}}" />
                <div class="{{{ $errors->has('attachment') ? 'has-error' : '' }}}">
                    <input type="file" name="image" class="filestyle" data-icon="false" data-buttonText="Choisir un fichier" placeholder="Pièce jointe" value="{{Input::old('attachment')}}" {{(isset($isDisabled)?$isDisabled:'')}}>
                    {{ $errors->first('attachment', '<div class="alert alert-danger">:message</div>') }}
                    </br>
                    <button class="btn btn-default" type="submit">Envoyer </button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div id="ModalProfileCouverture" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalProfileCouvertureLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <h2>Modification de votre photo de couverture</h2>
            <form action="{{ URL::route('photo-couverture')}}" method="POST" enctype="multipart/form-data" class="CoverPicture-form">
                <div><img src="{{$user->getPathPicture("couverture")}}" class="modal-image-cover"></div>
                </br>
                <input type="hidden" name="user_id" value="{{User::getIdByAuth()}}" />
                <div class="{{{ $errors->has('attachment') ? 'has-error' : '' }}}">
                    <input type="file" name="image" class="filestyle" data-icon="false" data-buttonText="Choisir un fichier" placeholder="Pièce jointe" value="{{Input::old('attachment')}}" {{(isset($isDisabled)?$isDisabled:'')}}>
                    {{ $errors->first('attachment', '<div class="alert alert-danger">:message</div>') }}
                    </br>
                    <button class="btn btn-default" type="submit">Envoyer </button>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>

<div id="Modalmotdepasse" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Modalmotdepasse" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body modal-password">
        <h2> Modification du mot de passe </h2>
        <form action="{{ URL::route('changement-mot-de-passe')}}" method="POST" enctype="multipart/form-data" class="passwordChange-form">
        </br>
        <div class="row">
                <div class="col-md-offset-1 col-md-8">
                <div class="form-group">
                <label for="Password">Ancien Mot de passe*</label>
                <input type="password" class="form-control" id="old-password" name="old-password" placeholder="Ancien Mot de passe">
                {{ $errors->first('password', '<div class="alert alert-danger">:message</div>') }}
                </div>
                </div>
        </div>
        </br>
            <div class="row">
                <div class="col-md-offset-1 col-md-8">
                <div class="form-group">
                <label for="Password">Nouveau Mot de passe*</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
                {{ $errors->first('password', '<div class="alert alert-danger">:message</div>') }}
                </div>
                </div>
            <div class="col-md-offset-1 col-md-8">
                <div class="form-group">
                <label for="Password">Vérification du Mot de passe*</label>
                <input type="password" class="form-control" id="vpassword" name="vpassword" placeholder="Vérification mot de passe">
                </div>
            </div>
    </div>
</br>

                    <button class="btn btn-default" type="submit">Valider </button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

</body>


@stop

</html>