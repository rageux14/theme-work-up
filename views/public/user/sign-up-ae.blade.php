@extends('theme::public.layout.master')

@section('content')

@include('theme::public.session.session-message')
<div class="container">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<h1> Inscription Auto-Entrepreneur<br/>
		    <small> Merci de renseigner vos informations </small></h1>
		    <div class="buttonAE">
				<a type="button" href="{{URL::to('sign-up')}}" class="btn btn-primary btn-lg">Inscription Client<span class="glyphicon glyphicon-menu-right"></span></a>
			</div>
		</div>
	</div>
	<br/>
	<br/>

	 @if (Session::has('error_login'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('error_login') }}
        </div>
    @endif

	{{ Form::open(array('action' => 'ModuleWorkUpRegisterController@add')) }}
	<input type="hidden"  name="typeInscription"  value="A">
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="form-group">
				<label for="Nom">Nom*</label>
				<input type="text" class="form-control" name="nom" value="{{Input::old('nom')}}" placeholder="Nom">
				{{ $errors->first('nom', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="form-group">
				<label for="Prenom">Prénom*</label>
				<input type="text" class="form-control" id="prenom" name="prenom" value="{{Input::old('prenom')}}"  placeholder="Prénom">
				{{ $errors->first('prenom', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="form-group">
    	<div class="col-md-offset-2 col-md-3">
            <div class="form-group">
            	<label for="Birthday">Date de Naissance</label>
                <div class='input-group date' id='birthday'>
                	<span class="input-group-addon glyphicon glyphicon-calendar"></span>
                    <input type='text' class="form-control text-center" id="date" name="date" value="{{Input::old('date')}}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
      	<div class="col-md-offset-1 col-md-3">
      		<label for="Gender">Sexe</label>
            <select class="form-control" id='sexe' name="sexe" value="{{Input::old('sexe')}}">
            	<option value="">Veuillez renseigner votre sexe...</option>
            	<option value="male">Homme</option>
                <option value="female">Femme</option>
            </select>
        </div>
    </div>
	</br>
	</div>

	<div class="row">
		<div class="col-md-offset-2 col-md-7">
			<div class="form-group">
				<label for="Email">Adresse Email*</label>
				<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{Input::old('email')}}">
				{{ $errors->first('email', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="form-group">
				<label for="Password">Mot de passe*</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
				{{ $errors->first('password', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="form-group">
				<label for="Vpassword">Vérification mot de passe*</label>
				<input type="password" class="form-control" id="vpassword" name="vpassword" placeholder="Vérification mot de passe">
				{{ $errors->first('vpassword', '<div class="alert alert-danger">:message</div>') }}
			</div>
		</div>
	</div>

	</br>

	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-earphone"></span>
				<input type="text" class="form-control" id="phone" name="phone" placeholder="Téléphone" aria-describedby="basic-addon1" value="{{Input::old('phone')}}">
			</div>
			{{ $errors->first('phone', '<div class="alert alert-danger">:message</div>') }}
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-globe"></span>
				<input type="text" class="form-control" name="adress" id="address" placeholder="Adresse" aria-describedby="basic-addon1" value="{{Input::old('adress')}}">
			</div>
			{{ $errors->first('adress', '<div class="alert alert-danger">:message</div>') }}
		</div>
	</div>

	</br>
	<div class="row">
		
		<div class="col-md-offset-2 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-home"></span>
				<input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Code Postal" aria-describedby="basic-addon1" value="{{Input::old('postal_code')}}">
			</div>
			{{ $errors->first('postal_code', '<div class="alert alert-danger">:message</div>') }}
		</div>
		<div class="col-md-offset-1 col-md-3">
			<div class="input-group">
				<span class="input-group-addon glyphicon glyphicon-home"></span>
				<input type="text" class="form-control" id="city" name="city" placeholder="Ville" aria-describedby="basic-addon1" value="{{Input::old('city')}}">
			</div>
			{{ $errors->first('city', '<div class="alert alert-danger">:message</div>') }}
		</div>
	</div>
	<br/>
	<div class="row">
      	<div class="col-md-offset-2 col-md-3">
      		<label for="categorie_metier">Catégorie métier</label>
      		<br/>
            <select class="form-control" name="categorie_metier" id="categorie_metier">
         		{{ JobCategory::getOptions( Input::old('categorie_metier') )}}
            </select>
        </div>
		<div class="col-md-offset-1 col-md-2">
			<div class="form-group">
				<label for="siren">Numéro de Siren*</label>
				<input type="text" class="form-control" id="siren" name="siren" placeholder="Siren" value="{{Input::old('siren')}}">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-2 col-md-6">
      		<label for="compagny">Nom de la Société*</label>
         	<input type="text" class="form-control" id="compagny" name="compagny" placeholder="Label" value="{{Input::old('compagny')}}">
        </div>
    </div>
</div>
	</br>
	<div class="row">
		<div class="col-md-offset-4 col-md-1">
			<button type="submit" class="btn btn-primary">Envoyer mes informations</button>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<a href="{{URL::to('sign-up')}}">Vous êtes un Client ?</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-2 col-md-3">
			<a href="{{URL::to('forgot-password')}}">Mot de passe oublié ?</a>
		</div>
	</div>
	<br/>
 
{{ Form::close() }}


@stop