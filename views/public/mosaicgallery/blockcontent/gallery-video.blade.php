<div class="cbp-fwslider cbp-fwslider-videos">
    <ul>
        @foreach ($gallery->videosByOrder as $video)
        <li data-thumb="{{ $video->getThumb() }}">
            <a>
                <iframe width="100%" height="467" src="{{ $video->getVideo() }}" frameborder="0" allowfullscreen></iframe>
            </a>
        </li>
        @endforeach
    </ul>
</div>