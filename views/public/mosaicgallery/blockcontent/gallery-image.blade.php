<div class="cbp-fwslider cbp-fwslider-images">
    <ul>
        @foreach ($gallery->imagesByOrder as $image)
        <li data-thumb="{{ $image->getThumb() }}" 
        @if(Request::segment(1) == 'admin')
            data-image-id="{{ $image->id }}"
            data-gallery-id="{{$gallery->id}}" 
        @endif
            >
            <a>
                <img u="image" src="{{ $image->getImage() }}" />
            </a>
            @if ($image->i18n_description != null)
            <span class="cbp-fwslider-info">
                {{$image->description()}}
            </span>
            @endif
        </li>
        @endforeach
    </ul>
</div>