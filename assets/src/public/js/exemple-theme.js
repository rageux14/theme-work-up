//Auth user system
var AuthUser = function() {
	//Variables
	var userId = null;

	//SignUp Modal
	this.signUpModal = function () {
		showSignUpModal();
	}
	this.signUpModalPreventDefault = function (e) {
		e.preventDefault();
		showSignUpModal();
	}
	this.setSignUpModal = function (f) {
		showSignUpModal = f;
	}

	//Login Modal
	this.loginModal = function () {
		showLoginModal();
	}
	this.loginModalPreventDefault = function (e) {
		e.preventDefault();
		showLoginModal();
	}
	this.setLoginModal = function (f) {
		console.log('coucou');
		showLoginModal = f;
	}

	//Sign up modal 
	var showSignUpModal = null;// set in a view (user componenets name of process)
		// offButtonSignUpModal
		this.offButtonSignUpModal = function () {
			offButtonSignUpModal();
		}
		var offButtonSignUpModal = function () {
			$('body').off('click', '.registration-button-login', showLoginModal);
		}


	//Login modal
	var showLoginModal = null;// set in a view (user componenets name of process)
		// offButtonLoginModal
		this.offButtonLoginModal = function () {
			offButtonLoginModal();
		}
		var offButtonLoginModal = function () {
			$('body').off('click', '.registration-button-sign-up', showSignUpModal);
		}


	//POST login
	var isPosting = null;
	
	this.submitAuth = function (e) {
		e.preventDefault();
		var form = $(e.target).closest('form');
		if (isPosting === null) {
			console.log(e);
			postAuth(form);
		}
	}
	
	var postAuth = function (form) {
		form.addClass('animate-submit');
		$.post(form.attr('action'), form.serializeArray(), function (response) {
			console.log(response);
			if ( response.statut == "success" ) {
				// Animate connexion
				form.closest('.modal-content').addClass('animate-connexion');

				// Put Hello you !
				// $('<h4 class="modal-title hello-you" style="display:none">Hello You !</h4>').insertAfter(form.closest('.authuser-modal-header').find('h4.modal-title'));
				// form.closest('.authuser-modal-header').find('.authuser-modal-title p.text-center').hide('fast');
				
				// Append avatar
				var avatarContainer = form.closest('.modal-content').find('span.icon-user-outline');
				var node = document.createDocumentFragment();
				var img = createElementWithClass('img', 'authuser-connexion-avatar growup');
					img.setAttribute('src', 'http://placehold.it/69x69');
					node.appendChild(img);
				avatarContainer.append(node);

				// Animate avatar
				var wait = setTimeout( function () {
					avatarContainer.find('img').addClass('');
					form.find('.hello-you').show('fast');
				}, 60);
				var wait = setTimeout( function () {
					//form.removeClass('animate-submit');
					//form.closest('.modal-content').removeClass('animate-connexion');
				}, 1000);

				showAlertInputMessage(form, response.statut, response.message);
			} else {
				form.addClass('animate-refused');
				form.find('button[type="submit"]').addClass('shakeit');
				var wait = setTimeout( function () {
					form.removeClass('animate-submit animate-refused');
					form.find('button[type="submit"]').removeClass('shakeit');
				}, 1000);

				showAlertInputMessage(form, response.statut, response.message);
			}
			isPosting = null;
			
			// Autosubmit
			if (ratingClass !== null) {
				ratingClass.autoSubmitRatingForm(response.user_id);
			}
		});
	}
	
	var showAlertInputMessage = function (form, type, message) {
		//Clean
		form.find('.authuser-alert').remove();

		var fragment = document.createDocumentFragment();
		var p = createElementWithClass('p', 'authuser-alert alert alert-' + type);
			p.textContent = message;
			fragment.appendChild(p);
		
		$(fragment).insertAfter(form.find('button[type="submit"]'));
	}

	var createElementWithClass = function (type, className){
        var object = document.createElement(type);
        object.className = className;
        if(arguments[2]){
            object.textContent = arguments[2];
        }
        return object;
    }
}
var authUser = null;

// Rating system
var Rating = function() {
	//Variable
	var ratingFormListId = [];

	this.start = function () {
		if (typeof($('input[name=rating]')) != 'undefined') {
			initListeners();
			initRatingForm();
		}
	}
	this.autoSubmitRatingForm = function (userId) {
		if (typeof($('input[name=user_id]')) != 'undefined') {
			$('input[name=user_id]').val(userId).submit();
		} else {
			console.log('Error auto-submit !');
		}
	}

	var initListeners = function () {
		//Listen change on input[name=rating] for passing to second step
	}

	var initRatingForm = function () {
		var form = null,
			history = [];
		
		// Listen element for step 1 to 2
		$('.rating-form-animated input[name=rating]').change(function (e) {
			setForm($(e.target).closest('form'));
			// Pass to step 2
			addHistory({step:1});
			upStep2();
			
			$(form).on('submit', submitRating);
		});

	}


	var postRatingForm = function (href, data) {
		return $.post(href, data, function (response) {
			//Show returned data
			console.log(response);
			if (response.test) {
				console.log('test');
			}
			console.log('test2');
			return response;
		});
	}
	this.postRatingForm = function (href, data) {
		return postRatingForm(href, data);
	}
	var submitRating = function (e) {
		e.preventDefault();
		//Show loader
		if (form.find('input[name=user_id]').val() == 'unlogged') {
			authUser.loginModal();
		} else {
			postRatingForm(form.attr('action'), {});
		}
	}

	var downStep1 = function () {
		form.addClass('step-1on3').removeClass('step-2on3');
		$('body').off('click', '.rating-button .button-return', downHistory);
		$(form.find('input[name*=rating_]')).off('change', stackRatingCategory);
		//$(form.find('input[name*=rating_]')).off('click', stackRatingCategory);
		$(form).off('submit', submitRating);
	}
	//var downStep2 = function () {}
	var upStep2 = function () {
		form.addClass('step-2on3').removeClass('step-1on3');
		$('body').on('click', '.rating-button .button-return', downHistory);
		$(form.find('input[name*=rating_]')).on('change', stackRatingCategory);
		//$(form.find('input[name*=rating_]')).on('click', stackRatingCategory);
	}
	var stackRatingCategory = function (e) {
		var me = $(e.target);
		var label = me.closest('.rating-category-label');
		label.addClass('rating-stars-collapse');
		addHistory({step:2, input:me.attr('name')});
		$(label).one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function(event) {
		    //label.addClass('hidden');
		});
	}
	var unstackRatingCategory = function (inputName) {
		var rate = form.find('input[name='+inputName+']');
		rate.closest('.rating-category-label').removeClass('rating-stars-collapse');
	}

	//History
	var addHistory = function (object) {
		history.push(object);
		//console.log(history);
	}
	var downHistory = function () {
		var historyIndex = history.length - 1;
		//console.log(history);
		if (history[historyIndex].step == 1) {
			downStep1();
		} else if (history[historyIndex].step == 2) {
			unstackRatingCategory(history[historyIndex].input);
		}
		history.splice(historyIndex,1);
		//console.log(history);
	}

	//Setter
	var setForm = function (data) {
		form = data;
		form.id = makeRandomIdAndStore();
	}

	this.getFormList = function () {
		return ratingFormListId;
	}

	//Detect if this from a rating-form ?
   /* var ratingForm = $(e.target).closest('.rating-form');
    if (typeof(ratingForm) != 'undefined') {
      //Faire ce callback si le form de login ou de signup se termine bien
      $('body').on('submit', 'form.login-form', function (e) {
        
      });
      $('body').on('submit', 'form.login-form', function (e) {
        
      });
      //callback();
    }*/

	var makeRandomIdAndStore = function () {
		var id = "rating-form-" + Math.floor((Math.random() * 1000) + 1);
		if (ratingFormListId.indexOf(id) == -1) {
			ratingFormListId.push(id);
		} else {
			makeRandomIdAndStore();
		}
		return id;
	}
}
// Rating system
var ratingClass = null;

var Splash = function () {
	var isPosting = null,
		formID = null,
		form = null,
		action = null;

	this.start = function () {
		formID = 'splash-form';
		form = $('#' + formID);
		action = form.attr('action');

		initListener(form, action);
	}
	
	var initListener = function () {
		//Listen
		$('body').on('submit', '#' + formID, function (e) {
			e.preventDefault();
			//post
			if (isPosting === null) {
				// Animate loader
				form.addClass('animate-submit');
				isPosting = $.post(action, form.serializeArray(), function (data) {
					if ( data.status == "success" ) {
						form.addClass('animate-response');
						form.find('input.awesome-input').val('');
						var wait = setTimeout( function () {
							form.removeClass('animate-submit animate-response');
						}, 1000);

						showAlertInputMessage(data.status, data.message);
					} else {
						form.addClass('animate-refused');
						var wait = setTimeout( function () {
							form.removeClass('animate-submit animate-refused');
						}, 1000);

						showAlertInputMessage(data.status, data.message);
					}
					isPosting = null;
				});
				
			}

		});
	}

	var showAlertInputMessage = function (type, message) {
		//Clean
		$('#' + formID + ' .splash-alert').remove();

		var fragment = document.createDocumentFragment();
		var p = createElementWithClass('p', 'splash-alert alert alert-' + type);
			p.textContent = message;
			fragment.appendChild(p);
		
		$(fragment).insertAfter($('#' + formID + ' .awesome-submit'));
	}

	var createElementWithClass = function (type, className){
        var object = document.createElement(type);
        object.className = className;
        if(arguments[2]){
            object.textContent = arguments[2];
        }
        return object;
    }
}
// Rating system
var splashClass = null;




//Object Master
var Master = function (){
	//Variables
	var queue = [];

	//Start
	this.start = function (){
		//get locale_id
		var localeDefault = 'fr';
		if (arguments[0]) {
			localeDefault = arguments[0];
		}
		//moment.initLocale(localeDefault);

		bootstrapModalTemplate();

		//Rating test
		if (typeof($('form.rating-form')) != 'undefined') {
			ratingClass = new Rating;
			ratingClass.start();
		}

		//Splash test
		if (typeof($('form.splash-form')) != 'undefined') {
			splashClass = new Splash;
			splashClass.start();
		}

		//Init connexion API ( Facebook ID)
		//facebookInit();

		//Init connexion API( Google + ID)
		//googlePlusInit();

		//Exec queue script (for module)
		execQueueScripts();


$( 'body' ).on('click', "#clickme", function() {
  alert( "Handler for .click() called." );
});
	}

	var bootstrapModalTemplate = function () {
		//Variable
	    var _defaultOptions = {
	        backdrop: "static",
	        close: true,
	        keyboard: true
	    };

	    window.bootstrap = {
	        modal: {
	            show: function (options) {
	                options = $.extend(_defaultOptions, options);
	               	var showFooter = (typeof(options.buttons) == 'undefined'?false:true);
	               	var dialogClass = (typeof(options.dialogClass) == 'undefined'?'':options.dialogClass);

				    var template = "<div id=\"bootstrap-modal\" class=\"modal fade\">" +
			                        "<div class=\"modal-dialog " + options.dialogClass + "\"><div class=\"modal-content\">" +
			                        "<div class=\"modal-header " + options.title.headerClass + "\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\">" +
			                        "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
			                        "{:Title}</div><div class=\"modal-body " + options.body.bodyClass + "\">{:Body}</div>";

			        if (showFooter) {
			        	template = template + "<div class=\"modal-footer\">{:Buttons}</div></div></div></div>";
			        }
			                        
	                var buttonText = "";

	                for (var key in options.buttons) {

	                    options.buttons[key].id = "button_" + key;

	                    var button = options.buttons[key];

	                    buttonText += "<button type=\"button\" " +
	                        "id=\"" + button.id + "\" " +
	                        "class=\"btn " +
	                        (typeof (button.class) == "undefined" ? "btn-default" : button.class) + "\" " +
	                        (typeof (button.dismiss) == "undefined" ? "" : "data-dismiss=\"modal\" ") + ">" +
	                        key + "</button>";
	                }

	                var setTemplate = function (template) {
	                    template = template
	                        .replace("{:Title}", options.title.content)
	                        .replace("{:Body}", options.body.content);

	                    if (showFooter) {
	                    	template.replace("{:Buttons}", buttonText);
	                    }

	                    var $modal = $("#bootstrap-modal");
	                    var existing = $modal.length;

	                    if (existing) {
	                        $modal.html($(template).find(".modal-dialog"));
	                    }
	                    else {
	                        $modal = $(template).appendTo("body");
	                    }

	                    if (showFooter) {
		                    for (var key in options.buttons) {
		                        var button = options.buttons[key];
		                        if (typeof (button.callback) !== undefined) {
		                            $("#" + button.id).on("click", button.callback);
		                        }
		                    }
	                    }

	                    $modal.off("shown.bs.modal").on("shown.bs.modal", function(e) {
	                        if (typeof(options.onshow) === "function") {
	                            options.onshow(e);
	                        }
	                    });
	                    
	                    if (!existing) {
	                        $modal.modal(options);
	                    }

	                    if (options.large === true) {
	                        $modal.find(".modal-dialog").addClass("modal-lg");
	                    }

	                    if (options.close === false) {
	                        $modal.find("button.close").remove();
	                    }
	                }
	                setTemplate(template);
	            },
	            hide: function (callback) {
	                //$("#bootstrap-modal").modal("hide");
	                var $modal = $("#bootstrap-modal");

	                if (!$modal.length) return;

	                $modal.on("hidden.bs.modal", function(e) {
	                    $modal.remove();
	                    if (typeof(callback) === "function") {
	                        callback(e);
	                    }
	                });

	                $modal.modal("hide");
	            }
	        }
	    }
	}


	//Queue Script system
	var execQueueScripts = function () {
		var q = getQueue();
		for( var o in q ) {
			console.log(q[o]);
			eval(q[o]);
		}
	}
	//Add a script to exec at masterClass.start()
	this.addQueue = function ( script ) {
		var q = getQueue();
		//console.log(q);
		
		q.push(script + ';');
		//console.log(q);
	}

	//return void
	var setQueue = function (a) {
		queue = a;
	}
	//return queue
	var getQueue = function () {
		return queue;
	}
}

var facebookInit = function () {
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '402914286558225',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
}

var googlePlusInit = function () {
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
}


var masterClass = new Master();


