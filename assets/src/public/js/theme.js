//Object Master

var Master = function (){

	//Start
	this.start = function (){
		//get locale_id
		initlistener();
		
	}
	var initlistener = function () {
        String.prototype.replaceAll = function(search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };

        $('body').on('click', '.addButtonLineDevis', function (e) {
            var line = $(this).parent().parent();
            var firstInput = line.find('input')[0];
            var index = $(firstInput).attr('name');
            var oldIndex = parseInt(index.substr(index.lastIndexOf('_') + 1));
            var futurIndex = oldIndex + 1;

            // Tempalte (duplicate HTML)
            var lineTemplate = '<div class="form-group"><label class="col-xs-1 control-label"></label><div class="col-xs-6"><input type="text" class="form-control" name="book_title_%%index%%" placeholder="Libelle" /></div><div class="col-xs-2"><input type="number" class="form-control" name="book_isbn_%%index%%" placeholder="P.U" /></div><div class="col-xs-2"><input type="number" class="form-control" name="book_price_%%index%%" placeholder="Qté" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/></div><div class="col-xs-1"><button type="button" class="btn btn-primary addButton addButtonLineDevis"><i class="glyphicon glyphicon-plus"></i></button></div></div>';
            var clone = lineTemplate.replaceAll("%%index%%", futurIndex);
            
            // Remove btn on prev line
            line.find('.addButtonLineDevis i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
            line.find('.addButtonLineDevis').on('click', function (e) {
                $(this).parent().parent().remove();
            });
            $(clone).insertAfter(line);
        });




		$('#date').datetimepicker({
            format: 'DD/MM/YYYY'
		});
        $('#datedebut').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#datefin').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#categorie_metier').multiselect({
                maxHeight: 200,
                enableFiltering: true,
                enableFullValueFiltering: true
            });

		$('#loginmodal').modal;

		$('body').on('click', "#clickme", function() {
  			alert( "Handler for .click() called." );
  		});

  		$('[type="radio"]').change( function(){
   	    	$('.choice').text( this.value + ' stars' );
		});

		
		var p = null;
		$('body').on('submit', "#form-benefit-ask", function (e) {
			e.preventDefault();
            $('#ModalPrestation').find('p.alert').remove();
			// FadeIn
			console.log('coucou');
			var me = $(this);

			if (p === null) {
				p = $.post(me.attr('action'), me.serializeArray(), function (response) {
					console.log(response);

					$('<p class="alert alert-' + response.statut + '">' + response.message + '</p>').insertAfter(me);
					
					if (response.statut == 'success') {
						// do job
						//$('<p class="alert alert-success">' + response.message + '</p>').insertAfter($('form.mestrucs-form').find('button[type="submit"]'));
					} else {
						// error
					}
					p = null;
				});
			}
		});

        $('body').on('click', "#ask-benefit", function (e) {
            $('#ModalPrestation').modal();
            $('#ModalPrestation').find('p.alert').remove();

        });

        var p = null;
        $('body').on('submit', "#form-contact-ask", function (e) {
            e.preventDefault();
            $('#ModalContact').find('p.alert').remove();
            // FadeIn
            console.log('coucou');
            var me = $(this);

            if (p === null) {
                p = $.post(me.attr('action'), me.serializeArray(), function (response) {
                    console.log(response);

                    $('<p class="alert alert-' + response.statut + '">' + response.message + '</p>').insertAfter(me);
                    
                    if (response.statut == 'success') {
                        // do job
                        //$('<p class="alert alert-success">' + response.message + '</p>').insertAfter($('form.mestrucs-form').find('button[type="submit"]'));
                    } else {
                        // error
                    }
                    p = null;
                });
            }
        });

        $('body').on('click', "#ask-contact", function (e) {
            $('#ModalContact').modal();
            $('#ModalContact').find('p.alert').remove();

        }); 

        var p = null;
        $('body').on('submit', "#send-devis", function (e) {
            e.preventDefault();
            $('#ModalDevisAnswer1').find('p.alert').remove();
            // FadeIn
            console.log('coucou');
            var me = $(this);

            if (p === null) {
                p = $.post(me.attr('action'), me.serializeArray(), function (response) {
                    console.log(response);

                    $('<p class="alert alert-' + response.statut + '">' + response.message + '</p>').insertAfter(me);
                    
                    if (response.statut == 'success') {
                        // do job
                        //$('<p class="alert alert-success">' + response.message + '</p>').insertAfter($('form.mestrucs-form').find('button[type="submit"]'));
                    } else {
                        // error
                    }
                    p = null;
                });
            }
        });

        $('body').on('click', "#send-devis", function (e) {
            $('#ModalDevisAnswer1').modal();
            $('#ModalDevisAnswer1').find('p.alert').remove();

        }); 
		//Add/Remove new input for DEVIS
    		var titleValidators = {
            row: '.col-xs-6',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'The title is required'
                }
            }
        },
        isbnValidators = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'The ISBN is required'
                },
                isbn: {
                    message: 'The ISBN is not valid'
                }
            }
        },
        priceValidators = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'The price is required'
                },
                numeric: {
                    message: 'The price must be a numeric number'
                }
            }
        },
        bookIndex = 0;

    $('#bookForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'book[0].title': titleValidators,
                'book[0].isbn': isbnValidators,
                'book[0].price': priceValidators
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            bookIndex++;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="title"]').attr('name', 'book[' + bookIndex + '].title').end()
                .find('[name="isbn"]').attr('name', 'book[' + bookIndex + '].isbn').end()
                .find('[name="price"]').attr('name', 'book[' + bookIndex + '].price').end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#bookForm')
                .formValidation('addField', 'book[' + bookIndex + '].title', titleValidators)
                .formValidation('addField', 'book[' + bookIndex + '].isbn', isbnValidators)
                .formValidation('addField', 'book[' + bookIndex + '].price', priceValidators)
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-book-index');

            // Remove fields
            $('#bookForm')
                .formValidation('removeField', $row.find('[name="book[' + index + '].title"]'))
                .formValidation('removeField', $row.find('[name="book[' + index + '].isbn"]'))
                .formValidation('removeField', $row.find('[name="book[' + index + '].price"]'));

            // Remove element containing the fields
            $row.remove();
        });

  	}
 
  //Queue Script system
	var execQueueScripts = function () {
		var q = getQueue();
		for( var o in q ) {
			console.log(q[o]);
			eval(q[o]);
		}
	}
	//Add a script to exec at masterClass.start()
	this.addQueue = function ( script ) {
		var q = getQueue();
		//console.log(q);
		
		q.push(script + ';');
		//console.log(q);
	}

	//return void
	var setQueue = function (a) {
		queue = a;
	}
	//return queue
	var getQueue = function () {
		return queue;
	}	
}



var masterClass = new Master();

//Remove + add textarea button "global-benefit"
function addBox(id) {
  		$('.mestrucs' + id).fadeIn('fast');
  		$('.supp-button' + id).remove();
  	}
//only number for Qté
$(document).ready(function() {
    $("#txtboxToFilter").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});


$(document).ready(function(){           
    $('input.amount').keyup(function(){   
        calculateTotal(this);
    });
});

function calculateTotal( src ) {
    var sum = 0,
        tbl = $(src).closest('table');
    tbl.find('input.form-control').each(function( index, elem ) {
        var val = parseFloat($(elem).val());
        if( !isNaN( val ) ) {
            sum += val;
        }
    });
    tbl.find('input.Totaldevis').html(sum.toFixed(2));
}