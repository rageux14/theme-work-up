module.exports = function (grunt) {
    // Code grunt
    grunt.initConfig({

        // Configuration du projet et des tâches
        pkg: grunt.file.readJSON('package.json'),

        // Paths
        srcPath: './src/',
        bowerPath: './bower_components/',
        vendorPath: './dist/vendor/',
        distPath: './dist/',
        deployPublicPath:'../../../public/',
        deployPath: '../../../public/theme/theme-work-up/',
        modulesPathVendor: './../../../vendor/dynamix/',
        modulesPathWorkbench: './../../../workbench/dynamix/',


        /**
         *
         *  CSS Tasks
         *
         */
        // Compilation
        compass: {
            //Public theme.css
            themePublic: {
              	options: {
                	config: '<%= srcPath %>public/config.rb',
                	sassDir: '<%= srcPath %>public/scss',
                	cssDir: '<%= distPath %>public/css'
            	}
            },
            //Vendor css
            /*vendorFontawesome: {
              options: {
                sassDir: '<%= bowerPath %>fontawesome/scss',
                cssDir: '<%= vendorPath %>css',
              }
            },*/

            //Module css
            modulesVendor: {
              options: {
                basePath: './../../..',
                sassDir: 'vendor/dynamix/*',
                cssDir: 'theme/theme-work-up/assets/dist/vendor/css/modules/modules',
              }
            },
            modulesWorkbench: {
              options: {
                basePath: './../../..',
                sassDir: 'workbench/dynamix/*',
                cssDir: 'theme/theme-work-up/assets/dist/vendor/css/modules/modules',
              }
            }
        },

        sass: {
            //Vendor css
            vendor: {
            	options: {
            		style: 'expanded'
            	},
                files: {
                    './dist/vendor/css/bootstrap.css': '<%= srcPath %>vendor/scss/bootstrap.scss'
                }
            }
        },

        cssmin:{
            combine: {
                files: {
                    // Main public CSS
                    '<%= distPath %>public/css/main.css':
                    [
                        //Vendor 
                        '<%= vendorPath %>css/workup-fontastic.css',
                        '<%= vendorPath %>css/bootstrap.css',
                        '<%= bowerPath %>sweetalert/dist/sweetalert.css',
                        '<%= bowerPath %>eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                        '<%= bowerPath %>bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
                        //Modules
                        '<%= vendorPath %>css/modules/**/public/**/*.css',
                        
                        //Theme
                        '<%= distPath %>public/css/theme.css',                                                                    
                    ]
                }
            },
            // Config de la tâche cssmin
            options: {

            }
        },



        /**
         *
         *  JS Tasks
         *
         */

        // Concatene les fchiers js
        concat: {
            options: {
              separator: ';\n',
            },
            assetPublic: {
                // Fichiers à concaténer
                src: [
                    //Vendor
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/transition.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/alert.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/button.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/carousel.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/collapse.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/dropdown.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/modal.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/tooltip.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/popover.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/scrollspy.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/tab.js',
                    '<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap/affix.js',
                    '<%= bowerPath %>bootstrap-filestyle/src/bootstrap-filestyle.min.js',
                    //Moment
                    '<%= bowerPath %>moment/moment.js',
                    //'<%= bowerPath %>moment/src/locale/fr.js',
                    '<%= bowerPath %>eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
                    '<%= bowerPath %>bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
                    '<%= bowerPath %>form.validation/src/js/base.js',
                    '<%= bowerPath %>form.validation/src/js/framework/bootstrap.js',
                    //'<%= bowerPath %>bootstrap/dist/js/bootstrap.min.js',
                    //'<%= bowerPath %>bootstrap-sass-twbs/assets/javascripts/bootstrap.js',
                    //'<%= bowerPath %>sweetalert/dist/sweetalert.min.js',
                    //'<%= bowerPath %>imagesloaded/imagesloaded.pkgd.min.js',

                    //Theme
                    '<%= srcPath %>public/js/theme.js',

                    //Modules
                    '<%= modulesPathVendor %>**/assets/public/js/**/*.js',
                    '<%= modulesPathWorkbench %>**/assets/public/js/**/*.js',
                ],
                // Fichier de destination
                dest:'<%= distPath %>public/js/main.js'
            }
        },

        // Minification
        uglify: {
            options: {
                // la date et le nom des fichiers minifiés sont insérés en commentaire en début de fichier
                banner:'/* <%= grunt.template.today("dd-mm-yyyy, HH:MM") %> */\n'
            },

            assetPublic: {
                files: {
                    // Fichier de destination
                    '<%= concat.assetPublic.dest %>':
                    // Fichier minifié
                    ['<%= concat.assetPublic.dest %>']
                }
            }
        },



        /**
         *
         *  Other Tasks
         *
         */

        // Hashage des fichiers minimifié
        hash: {
            options: {
                mapping: '../../../app/config/assets/theme/public-theme-work-up.json', //mapping file so your server can serve the right files
                srcBasePath: '<%= distPath %>', // the base Path you want to remove from the `key` string in the mapping file
                destBasePath: '<%= distPath %>', // the base Path you want to remove from the `value` string in the mapping file
                flatten: false // Set to true if you don't want to keep folder structure in the `key` value in the mapping file
            },
            jsPublic: {
                src: ['<%= distPath %>public/js/main.js'],
                dest: '<%= distPath %>public/js'
            },
            cssPublic: {
                src: ['<%= distPath %>public/css/main.css'],
                dest: '<%= distPath %>public/css'
            }
        },


        // Nettoyage des dossiers publics
        clean: {
            jspublic: ["<%= distPath %>public/css/*.js"],
            csspublic: ["<%= distPath %>public/css/*.css"],
            options: {
                force: true
            }
        },


        copy: {
          vendor: {
            files: [
              // Modernizr
              {expand: false, src: ['<%= srcPath %>vendor/js/modernizr.min.js'], dest: '<%= distPath %>public/js/vendor/modernizr.min.js', filter: 'isFile'},
              
              // jQuery
              {expand: false, src: ['<%= bowerPath %>jquery/dist/jquery.min.js'], dest: '<%= distPath %>public/js/vendor/jquery.min.js', filter: 'isFile'},
              {expand: false, src: ['<%= bowerPath %>jquery/dist/jquery.min.map'], dest: '<%= distPath %>public/js/vendor/jquery.min.map', filter: 'isFile'},

              // HeadJS
              {expand: false, src: ['<%= bowerPath %>headjs/dist/1.0.0/head.min.js'], dest: '<%= distPath %>public/js/vendor/head.min.js', filter: 'isFile'},
              {expand: false, src: ['<%= bowerPath %>headjs/dist/1.0.0/head.min.js.map'], dest: '<%= distPath %>public/js/vendor/head.min.js.map', filter: 'isFile'},

              // ResponseJS
              {expand: false, src: ['<%= bowerPath %>responsejs/response.min.js'], dest: '<%= distPath %>public/js/vendor/response.min.js', filter: 'isFile'},

              // Media-match
              {expand: false, src: ['<%= bowerPath %>media-match/media.match.min.js'], dest: '<%= distPath %>public/js/vendor/media.match.min.js', filter: 'isFile'},

              // CKEditor
              {expand: true, cwd: '<%= bowerPath %>ckeditor/', src: ['**'], dest: '<%= deployPublicPath %>js/ckeditor/'},
            ]
          },
          fonts: {
            files: [ 
              //Fonts
              {expand: true, src: ['<%= srcPath %>public/fonts/vendor/*'], dest: '<%= distPath %>public/fonts/vendor/', flatten: true},
              {expand: true, src: ['<%= srcPath %>public/fonts/workup/*'], dest: '<%= distPath %>public/fonts/workup/', flatten: true},
              {expand: true, src: ['<%= bowerPath %>bootstrap-sass-twbs/assets/fonts/bootstrap/*'], dest: '<%= distPath %>public/fonts/bootstrap/', flatten: true},
            ]
          },
          images: {
            files: [
              //Images
              {expand: true, cwd: '<%= srcPath %>public/img/sources/', src: ['**'], dest: '<%= distPath %>public/img/'},
              {expand: true, cwd: '<%= srcPath %>public/img/favicon/', src: ['**'], dest: '<%= deployPublicPath %>uploads/system/favicon/'},
              {expand: true, cwd: '<%= srcPath %>public/img/logo/', src: ['**'], dest: '<%= distPath %>public/img/logo/'},
              {expand: true, cwd: '<%= srcPath %>public/img/default-picture/', src: ['**'], dest: '<%= deployPublicPath %>uploads/system/default-picture/'},
            ]
          },
          deploy: {
            files: [           
              {expand: true, cwd: '<%= distPath %>public/', src: ['**'], dest: '<%= deployPath %>public/'}          
            ]
          }
        },

        // Watchs
        watch: {
              themePublicSCSS: 
            {
                files: [
                        '<%= srcPath %>public/scss/**'
                ],
                tasks:['clean:csspublic', 'compass:themePublic', 'cssmin', 'hash:cssPublic', 'copy:deploy'],
                options: {
                  livereload: true
                }    
            },                   

            themePublicJS: 
            {
                files: [
                        '<%= srcPath %>public/js/**'
                ],
                tasks:['concat:assetPublic', 'clean:jspublic', 'hash:jsPublic', 'copy:deploy'],
                options: {
                  livereload: true
                }    
            },                   

            blade: 
            {
                files: ['<%= path %>app/**/*.blade.php'],
                options: {
                  livereload: true
                }    
            }
        }        
    });

    // Chargement des plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-hash');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Tâches par défauts
    grunt.registerTask('default', ['clean', 'sass', 'compass', 'cssmin', 'concat', 'hash', 'copy', 'watch']);


    // Tâches personnalisées pour le développement
    grunt.registerTask('dev', ['clean', 'sass', 'compass', 'cssmin', 'concat', 'hash', 'copy', 'watch']);

	// Tâches personnalisées pour le développement
    grunt.registerTask('deploy', ['copy:deploy']);

    // Tâches personnalisées pour la mise en prod
    grunt.registerTask('prod', ['clean', 'sass', 'compass', 'cssmin', 'concat', 'hash', 'copy']);

}
