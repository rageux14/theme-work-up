<?php

class ThemeWorkUpDatabaseSeeder extends Seeder {

    public function run()
    {
    	// Put theme in db
        DB::table('themes')->insert( array(
            array(
                'name'    	=> 'theme-work-up',
                'type'    	=> 'public',
                'active'    	=> true
            ))
        );

        $defaultPublic = Theme::where('type', 'public')->where('name', 'default')->first();
        $defaultPublic->active = false;
        $defaultPublic->save();
    }

}